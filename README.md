# w2ui_grid_gitlab_pages

~~Combine [w2ui](http://w2ui.com/web/home) grid with gitlab pages.~~

~~The w2ui table is based on this example: http://w2ui.com/web/demos/#!grid/grid-1.~~

Combine [DataTables](https://datatables.net/) with gitlab pages.

The json data for the table is accessed through a publicly available link: 
https://api.jsonbin.io/b/5fb6a86702f80c2af522c8ed.

## Local build
To build the `mkdocs` site locally:

```commandline
mkdocs build
```

This generates the complete site and puts the files into a folder named `public`.
It is not updated automatically when you make a change but you have to rerun `mkdocs
 build`.
 
`mkdocs serve` that produces an automatically updated local build does not work with
 DataTables (for example, see [this related issue](https://github.com/mkdocs/mkdocs/issues/1351)).

## Deployment (GitLab pages)

#### master

At the moment GitLab Pages does not allow multiple live versions so we decided to
 have it set up with the `master` branch. You can find [the live documentation here
 ](https://rozsasarpi.gitlab.io/w2ui_grid_gitlab_pages/).
 
#### != master

In order to facilitate testing we also generate the documentation from all non-master
 branches. These are accessible [as environments from here](https://gitlab.com/rozsasarpi/w2ui_grid_gitlab_pages/-/environments) and clicking on the `Open live environment` button.